build-server:
	echo "Compiling Ku Eden API Server binary"
	go build -o ./bin/kueden-api-server ./cmd/server/main.go

build-dev:
	echo "Compiling Ku Eden API Server binary for development/test"
	go build -o ./tmp/kueden-api-server ./cmd/server/main.go

clean-dev:
	echo "Cleaning development built files"
	rm -rf ./tmp/*

clean-all:
	echo "Cleaning all built files"
	rm -rf ./tmp/*
	rm -rf ./bin/*

run-dev:
	air

all: clean-all build-server
