package service

import (
	"context"
	"go-echo-ent/component"
	"go-echo-ent/ent"
)

type RepositoryOps struct {
	ctx    context.Context
	client *ent.Client
}

func NewRepositoryOps(ctx context.Context) *RepositoryOps {
	return &RepositoryOps{
		ctx:    ctx,
		client: component.GetEntClient(),
	}
}
