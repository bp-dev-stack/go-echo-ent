package service

import (
	"go-echo-ent/ent"
	"go-echo-ent/ent/serverconfig"
)

func (r *RepositoryOps) ServerConfigGetAll() ([]*ent.ServerConfig, error) {

	svrConfigs, err := r.client.ServerConfig.Query().Order(ent.Asc(serverconfig.FieldCid)).All(r.ctx)
	if err != nil {
		return nil, err
	}

	return svrConfigs, nil
}
