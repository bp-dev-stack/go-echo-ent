// Code generated by ent, DO NOT EDIT.

package ent

import (
	"context"
	"fmt"
	"go-echo-ent/ent/predicate"
	"go-echo-ent/ent/serverconfig"
	"math"

	"entgo.io/ent/dialect/sql"
	"entgo.io/ent/dialect/sql/sqlgraph"
	"entgo.io/ent/schema/field"
)

// ServerConfigQuery is the builder for querying ServerConfig entities.
type ServerConfigQuery struct {
	config
	limit      *int
	offset     *int
	unique     *bool
	order      []OrderFunc
	fields     []string
	predicates []predicate.ServerConfig
	// intermediate query (i.e. traversal path).
	sql  *sql.Selector
	path func(context.Context) (*sql.Selector, error)
}

// Where adds a new predicate for the ServerConfigQuery builder.
func (scq *ServerConfigQuery) Where(ps ...predicate.ServerConfig) *ServerConfigQuery {
	scq.predicates = append(scq.predicates, ps...)
	return scq
}

// Limit adds a limit step to the query.
func (scq *ServerConfigQuery) Limit(limit int) *ServerConfigQuery {
	scq.limit = &limit
	return scq
}

// Offset adds an offset step to the query.
func (scq *ServerConfigQuery) Offset(offset int) *ServerConfigQuery {
	scq.offset = &offset
	return scq
}

// Unique configures the query builder to filter duplicate records on query.
// By default, unique is set to true, and can be disabled using this method.
func (scq *ServerConfigQuery) Unique(unique bool) *ServerConfigQuery {
	scq.unique = &unique
	return scq
}

// Order adds an order step to the query.
func (scq *ServerConfigQuery) Order(o ...OrderFunc) *ServerConfigQuery {
	scq.order = append(scq.order, o...)
	return scq
}

// First returns the first ServerConfig entity from the query.
// Returns a *NotFoundError when no ServerConfig was found.
func (scq *ServerConfigQuery) First(ctx context.Context) (*ServerConfig, error) {
	nodes, err := scq.Limit(1).All(ctx)
	if err != nil {
		return nil, err
	}
	if len(nodes) == 0 {
		return nil, &NotFoundError{serverconfig.Label}
	}
	return nodes[0], nil
}

// FirstX is like First, but panics if an error occurs.
func (scq *ServerConfigQuery) FirstX(ctx context.Context) *ServerConfig {
	node, err := scq.First(ctx)
	if err != nil && !IsNotFound(err) {
		panic(err)
	}
	return node
}

// FirstID returns the first ServerConfig ID from the query.
// Returns a *NotFoundError when no ServerConfig ID was found.
func (scq *ServerConfigQuery) FirstID(ctx context.Context) (id int, err error) {
	var ids []int
	if ids, err = scq.Limit(1).IDs(ctx); err != nil {
		return
	}
	if len(ids) == 0 {
		err = &NotFoundError{serverconfig.Label}
		return
	}
	return ids[0], nil
}

// FirstIDX is like FirstID, but panics if an error occurs.
func (scq *ServerConfigQuery) FirstIDX(ctx context.Context) int {
	id, err := scq.FirstID(ctx)
	if err != nil && !IsNotFound(err) {
		panic(err)
	}
	return id
}

// Only returns a single ServerConfig entity found by the query, ensuring it only returns one.
// Returns a *NotSingularError when more than one ServerConfig entity is found.
// Returns a *NotFoundError when no ServerConfig entities are found.
func (scq *ServerConfigQuery) Only(ctx context.Context) (*ServerConfig, error) {
	nodes, err := scq.Limit(2).All(ctx)
	if err != nil {
		return nil, err
	}
	switch len(nodes) {
	case 1:
		return nodes[0], nil
	case 0:
		return nil, &NotFoundError{serverconfig.Label}
	default:
		return nil, &NotSingularError{serverconfig.Label}
	}
}

// OnlyX is like Only, but panics if an error occurs.
func (scq *ServerConfigQuery) OnlyX(ctx context.Context) *ServerConfig {
	node, err := scq.Only(ctx)
	if err != nil {
		panic(err)
	}
	return node
}

// OnlyID is like Only, but returns the only ServerConfig ID in the query.
// Returns a *NotSingularError when more than one ServerConfig ID is found.
// Returns a *NotFoundError when no entities are found.
func (scq *ServerConfigQuery) OnlyID(ctx context.Context) (id int, err error) {
	var ids []int
	if ids, err = scq.Limit(2).IDs(ctx); err != nil {
		return
	}
	switch len(ids) {
	case 1:
		id = ids[0]
	case 0:
		err = &NotFoundError{serverconfig.Label}
	default:
		err = &NotSingularError{serverconfig.Label}
	}
	return
}

// OnlyIDX is like OnlyID, but panics if an error occurs.
func (scq *ServerConfigQuery) OnlyIDX(ctx context.Context) int {
	id, err := scq.OnlyID(ctx)
	if err != nil {
		panic(err)
	}
	return id
}

// All executes the query and returns a list of ServerConfigs.
func (scq *ServerConfigQuery) All(ctx context.Context) ([]*ServerConfig, error) {
	if err := scq.prepareQuery(ctx); err != nil {
		return nil, err
	}
	return scq.sqlAll(ctx)
}

// AllX is like All, but panics if an error occurs.
func (scq *ServerConfigQuery) AllX(ctx context.Context) []*ServerConfig {
	nodes, err := scq.All(ctx)
	if err != nil {
		panic(err)
	}
	return nodes
}

// IDs executes the query and returns a list of ServerConfig IDs.
func (scq *ServerConfigQuery) IDs(ctx context.Context) ([]int, error) {
	var ids []int
	if err := scq.Select(serverconfig.FieldID).Scan(ctx, &ids); err != nil {
		return nil, err
	}
	return ids, nil
}

// IDsX is like IDs, but panics if an error occurs.
func (scq *ServerConfigQuery) IDsX(ctx context.Context) []int {
	ids, err := scq.IDs(ctx)
	if err != nil {
		panic(err)
	}
	return ids
}

// Count returns the count of the given query.
func (scq *ServerConfigQuery) Count(ctx context.Context) (int, error) {
	if err := scq.prepareQuery(ctx); err != nil {
		return 0, err
	}
	return scq.sqlCount(ctx)
}

// CountX is like Count, but panics if an error occurs.
func (scq *ServerConfigQuery) CountX(ctx context.Context) int {
	count, err := scq.Count(ctx)
	if err != nil {
		panic(err)
	}
	return count
}

// Exist returns true if the query has elements in the graph.
func (scq *ServerConfigQuery) Exist(ctx context.Context) (bool, error) {
	if err := scq.prepareQuery(ctx); err != nil {
		return false, err
	}
	return scq.sqlExist(ctx)
}

// ExistX is like Exist, but panics if an error occurs.
func (scq *ServerConfigQuery) ExistX(ctx context.Context) bool {
	exist, err := scq.Exist(ctx)
	if err != nil {
		panic(err)
	}
	return exist
}

// Clone returns a duplicate of the ServerConfigQuery builder, including all associated steps. It can be
// used to prepare common query builders and use them differently after the clone is made.
func (scq *ServerConfigQuery) Clone() *ServerConfigQuery {
	if scq == nil {
		return nil
	}
	return &ServerConfigQuery{
		config:     scq.config,
		limit:      scq.limit,
		offset:     scq.offset,
		order:      append([]OrderFunc{}, scq.order...),
		predicates: append([]predicate.ServerConfig{}, scq.predicates...),
		// clone intermediate query.
		sql:    scq.sql.Clone(),
		path:   scq.path,
		unique: scq.unique,
	}
}

// GroupBy is used to group vertices by one or more fields/columns.
// It is often used with aggregate functions, like: count, max, mean, min, sum.
//
// Example:
//
//	var v []struct {
//		Cid string `json:"cid,omitempty"`
//		Count int `json:"count,omitempty"`
//	}
//
//	client.ServerConfig.Query().
//		GroupBy(serverconfig.FieldCid).
//		Aggregate(ent.Count()).
//		Scan(ctx, &v)
func (scq *ServerConfigQuery) GroupBy(field string, fields ...string) *ServerConfigGroupBy {
	grbuild := &ServerConfigGroupBy{config: scq.config}
	grbuild.fields = append([]string{field}, fields...)
	grbuild.path = func(ctx context.Context) (prev *sql.Selector, err error) {
		if err := scq.prepareQuery(ctx); err != nil {
			return nil, err
		}
		return scq.sqlQuery(ctx), nil
	}
	grbuild.label = serverconfig.Label
	grbuild.flds, grbuild.scan = &grbuild.fields, grbuild.Scan
	return grbuild
}

// Select allows the selection one or more fields/columns for the given query,
// instead of selecting all fields in the entity.
//
// Example:
//
//	var v []struct {
//		Cid string `json:"cid,omitempty"`
//	}
//
//	client.ServerConfig.Query().
//		Select(serverconfig.FieldCid).
//		Scan(ctx, &v)
func (scq *ServerConfigQuery) Select(fields ...string) *ServerConfigSelect {
	scq.fields = append(scq.fields, fields...)
	selbuild := &ServerConfigSelect{ServerConfigQuery: scq}
	selbuild.label = serverconfig.Label
	selbuild.flds, selbuild.scan = &scq.fields, selbuild.Scan
	return selbuild
}

// Aggregate returns a ServerConfigSelect configured with the given aggregations.
func (scq *ServerConfigQuery) Aggregate(fns ...AggregateFunc) *ServerConfigSelect {
	return scq.Select().Aggregate(fns...)
}

func (scq *ServerConfigQuery) prepareQuery(ctx context.Context) error {
	for _, f := range scq.fields {
		if !serverconfig.ValidColumn(f) {
			return &ValidationError{Name: f, err: fmt.Errorf("ent: invalid field %q for query", f)}
		}
	}
	if scq.path != nil {
		prev, err := scq.path(ctx)
		if err != nil {
			return err
		}
		scq.sql = prev
	}
	return nil
}

func (scq *ServerConfigQuery) sqlAll(ctx context.Context, hooks ...queryHook) ([]*ServerConfig, error) {
	var (
		nodes = []*ServerConfig{}
		_spec = scq.querySpec()
	)
	_spec.ScanValues = func(columns []string) ([]any, error) {
		return (*ServerConfig).scanValues(nil, columns)
	}
	_spec.Assign = func(columns []string, values []any) error {
		node := &ServerConfig{config: scq.config}
		nodes = append(nodes, node)
		return node.assignValues(columns, values)
	}
	for i := range hooks {
		hooks[i](ctx, _spec)
	}
	if err := sqlgraph.QueryNodes(ctx, scq.driver, _spec); err != nil {
		return nil, err
	}
	if len(nodes) == 0 {
		return nodes, nil
	}
	return nodes, nil
}

func (scq *ServerConfigQuery) sqlCount(ctx context.Context) (int, error) {
	_spec := scq.querySpec()
	_spec.Node.Columns = scq.fields
	if len(scq.fields) > 0 {
		_spec.Unique = scq.unique != nil && *scq.unique
	}
	return sqlgraph.CountNodes(ctx, scq.driver, _spec)
}

func (scq *ServerConfigQuery) sqlExist(ctx context.Context) (bool, error) {
	switch _, err := scq.FirstID(ctx); {
	case IsNotFound(err):
		return false, nil
	case err != nil:
		return false, fmt.Errorf("ent: check existence: %w", err)
	default:
		return true, nil
	}
}

func (scq *ServerConfigQuery) querySpec() *sqlgraph.QuerySpec {
	_spec := &sqlgraph.QuerySpec{
		Node: &sqlgraph.NodeSpec{
			Table:   serverconfig.Table,
			Columns: serverconfig.Columns,
			ID: &sqlgraph.FieldSpec{
				Type:   field.TypeInt,
				Column: serverconfig.FieldID,
			},
		},
		From:   scq.sql,
		Unique: true,
	}
	if unique := scq.unique; unique != nil {
		_spec.Unique = *unique
	}
	if fields := scq.fields; len(fields) > 0 {
		_spec.Node.Columns = make([]string, 0, len(fields))
		_spec.Node.Columns = append(_spec.Node.Columns, serverconfig.FieldID)
		for i := range fields {
			if fields[i] != serverconfig.FieldID {
				_spec.Node.Columns = append(_spec.Node.Columns, fields[i])
			}
		}
	}
	if ps := scq.predicates; len(ps) > 0 {
		_spec.Predicate = func(selector *sql.Selector) {
			for i := range ps {
				ps[i](selector)
			}
		}
	}
	if limit := scq.limit; limit != nil {
		_spec.Limit = *limit
	}
	if offset := scq.offset; offset != nil {
		_spec.Offset = *offset
	}
	if ps := scq.order; len(ps) > 0 {
		_spec.Order = func(selector *sql.Selector) {
			for i := range ps {
				ps[i](selector)
			}
		}
	}
	return _spec
}

func (scq *ServerConfigQuery) sqlQuery(ctx context.Context) *sql.Selector {
	builder := sql.Dialect(scq.driver.Dialect())
	t1 := builder.Table(serverconfig.Table)
	columns := scq.fields
	if len(columns) == 0 {
		columns = serverconfig.Columns
	}
	selector := builder.Select(t1.Columns(columns...)...).From(t1)
	if scq.sql != nil {
		selector = scq.sql
		selector.Select(selector.Columns(columns...)...)
	}
	if scq.unique != nil && *scq.unique {
		selector.Distinct()
	}
	for _, p := range scq.predicates {
		p(selector)
	}
	for _, p := range scq.order {
		p(selector)
	}
	if offset := scq.offset; offset != nil {
		// limit is mandatory for offset clause. We start
		// with default value, and override it below if needed.
		selector.Offset(*offset).Limit(math.MaxInt32)
	}
	if limit := scq.limit; limit != nil {
		selector.Limit(*limit)
	}
	return selector
}

// ServerConfigGroupBy is the group-by builder for ServerConfig entities.
type ServerConfigGroupBy struct {
	config
	selector
	fields []string
	fns    []AggregateFunc
	// intermediate query (i.e. traversal path).
	sql  *sql.Selector
	path func(context.Context) (*sql.Selector, error)
}

// Aggregate adds the given aggregation functions to the group-by query.
func (scgb *ServerConfigGroupBy) Aggregate(fns ...AggregateFunc) *ServerConfigGroupBy {
	scgb.fns = append(scgb.fns, fns...)
	return scgb
}

// Scan applies the group-by query and scans the result into the given value.
func (scgb *ServerConfigGroupBy) Scan(ctx context.Context, v any) error {
	query, err := scgb.path(ctx)
	if err != nil {
		return err
	}
	scgb.sql = query
	return scgb.sqlScan(ctx, v)
}

func (scgb *ServerConfigGroupBy) sqlScan(ctx context.Context, v any) error {
	for _, f := range scgb.fields {
		if !serverconfig.ValidColumn(f) {
			return &ValidationError{Name: f, err: fmt.Errorf("invalid field %q for group-by", f)}
		}
	}
	selector := scgb.sqlQuery()
	if err := selector.Err(); err != nil {
		return err
	}
	rows := &sql.Rows{}
	query, args := selector.Query()
	if err := scgb.driver.Query(ctx, query, args, rows); err != nil {
		return err
	}
	defer rows.Close()
	return sql.ScanSlice(rows, v)
}

func (scgb *ServerConfigGroupBy) sqlQuery() *sql.Selector {
	selector := scgb.sql.Select()
	aggregation := make([]string, 0, len(scgb.fns))
	for _, fn := range scgb.fns {
		aggregation = append(aggregation, fn(selector))
	}
	if len(selector.SelectedColumns()) == 0 {
		columns := make([]string, 0, len(scgb.fields)+len(scgb.fns))
		for _, f := range scgb.fields {
			columns = append(columns, selector.C(f))
		}
		columns = append(columns, aggregation...)
		selector.Select(columns...)
	}
	return selector.GroupBy(selector.Columns(scgb.fields...)...)
}

// ServerConfigSelect is the builder for selecting fields of ServerConfig entities.
type ServerConfigSelect struct {
	*ServerConfigQuery
	selector
	// intermediate query (i.e. traversal path).
	sql *sql.Selector
}

// Aggregate adds the given aggregation functions to the selector query.
func (scs *ServerConfigSelect) Aggregate(fns ...AggregateFunc) *ServerConfigSelect {
	scs.fns = append(scs.fns, fns...)
	return scs
}

// Scan applies the selector query and scans the result into the given value.
func (scs *ServerConfigSelect) Scan(ctx context.Context, v any) error {
	if err := scs.prepareQuery(ctx); err != nil {
		return err
	}
	scs.sql = scs.ServerConfigQuery.sqlQuery(ctx)
	return scs.sqlScan(ctx, v)
}

func (scs *ServerConfigSelect) sqlScan(ctx context.Context, v any) error {
	aggregation := make([]string, 0, len(scs.fns))
	for _, fn := range scs.fns {
		aggregation = append(aggregation, fn(scs.sql))
	}
	switch n := len(*scs.selector.flds); {
	case n == 0 && len(aggregation) > 0:
		scs.sql.Select(aggregation...)
	case n != 0 && len(aggregation) > 0:
		scs.sql.AppendSelect(aggregation...)
	}
	rows := &sql.Rows{}
	query, args := scs.sql.Query()
	if err := scs.driver.Query(ctx, query, args, rows); err != nil {
		return err
	}
	defer rows.Close()
	return sql.ScanSlice(rows, v)
}
