package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/field"
)

// ServerConfigs holds the schema definition for the ServerConfigs entity.
type ServerConfig struct {
	ent.Schema
}

// Fields of the ServerConfigs.
func (ServerConfig) Fields() []ent.Field {
	return []ent.Field{
		field.String("cid").
			MaxLen(32).
			NotEmpty().
			Unique(),
		field.String("description").
			MaxLen(64).
			Optional(),
		field.String("value").
			MaxLen(255).
			NotEmpty().
			Default(""),
	}
}

// Edges of the ServerConfigs.
func (ServerConfig) Edges() []ent.Edge {
	return nil
}
