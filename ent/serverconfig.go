// Code generated by ent, DO NOT EDIT.

package ent

import (
	"fmt"
	"go-echo-ent/ent/serverconfig"
	"strings"

	"entgo.io/ent/dialect/sql"
)

// ServerConfig is the model entity for the ServerConfig schema.
type ServerConfig struct {
	config `json:"-"`
	// ID of the ent.
	ID int `json:"id,omitempty"`
	// Cid holds the value of the "cid" field.
	Cid string `json:"cid,omitempty"`
	// Description holds the value of the "description" field.
	Description string `json:"description,omitempty"`
	// Value holds the value of the "value" field.
	Value string `json:"value,omitempty"`
}

// scanValues returns the types for scanning values from sql.Rows.
func (*ServerConfig) scanValues(columns []string) ([]any, error) {
	values := make([]any, len(columns))
	for i := range columns {
		switch columns[i] {
		case serverconfig.FieldID:
			values[i] = new(sql.NullInt64)
		case serverconfig.FieldCid, serverconfig.FieldDescription, serverconfig.FieldValue:
			values[i] = new(sql.NullString)
		default:
			return nil, fmt.Errorf("unexpected column %q for type ServerConfig", columns[i])
		}
	}
	return values, nil
}

// assignValues assigns the values that were returned from sql.Rows (after scanning)
// to the ServerConfig fields.
func (sc *ServerConfig) assignValues(columns []string, values []any) error {
	if m, n := len(values), len(columns); m < n {
		return fmt.Errorf("mismatch number of scan values: %d != %d", m, n)
	}
	for i := range columns {
		switch columns[i] {
		case serverconfig.FieldID:
			value, ok := values[i].(*sql.NullInt64)
			if !ok {
				return fmt.Errorf("unexpected type %T for field id", value)
			}
			sc.ID = int(value.Int64)
		case serverconfig.FieldCid:
			if value, ok := values[i].(*sql.NullString); !ok {
				return fmt.Errorf("unexpected type %T for field cid", values[i])
			} else if value.Valid {
				sc.Cid = value.String
			}
		case serverconfig.FieldDescription:
			if value, ok := values[i].(*sql.NullString); !ok {
				return fmt.Errorf("unexpected type %T for field description", values[i])
			} else if value.Valid {
				sc.Description = value.String
			}
		case serverconfig.FieldValue:
			if value, ok := values[i].(*sql.NullString); !ok {
				return fmt.Errorf("unexpected type %T for field value", values[i])
			} else if value.Valid {
				sc.Value = value.String
			}
		}
	}
	return nil
}

// Update returns a builder for updating this ServerConfig.
// Note that you need to call ServerConfig.Unwrap() before calling this method if this ServerConfig
// was returned from a transaction, and the transaction was committed or rolled back.
func (sc *ServerConfig) Update() *ServerConfigUpdateOne {
	return (&ServerConfigClient{config: sc.config}).UpdateOne(sc)
}

// Unwrap unwraps the ServerConfig entity that was returned from a transaction after it was closed,
// so that all future queries will be executed through the driver which created the transaction.
func (sc *ServerConfig) Unwrap() *ServerConfig {
	_tx, ok := sc.config.driver.(*txDriver)
	if !ok {
		panic("ent: ServerConfig is not a transactional entity")
	}
	sc.config.driver = _tx.drv
	return sc
}

// String implements the fmt.Stringer.
func (sc *ServerConfig) String() string {
	var builder strings.Builder
	builder.WriteString("ServerConfig(")
	builder.WriteString(fmt.Sprintf("id=%v, ", sc.ID))
	builder.WriteString("cid=")
	builder.WriteString(sc.Cid)
	builder.WriteString(", ")
	builder.WriteString("description=")
	builder.WriteString(sc.Description)
	builder.WriteString(", ")
	builder.WriteString("value=")
	builder.WriteString(sc.Value)
	builder.WriteByte(')')
	return builder.String()
}

// ServerConfigs is a parsable slice of ServerConfig.
type ServerConfigs []*ServerConfig

func (sc ServerConfigs) config(cfg config) {
	for _i := range sc {
		sc[_i].config = cfg
	}
}
