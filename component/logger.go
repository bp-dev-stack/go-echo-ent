package component

import (
	"os"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var Logger *zap.Logger

func InitializeLogger() {
	logLevel, ok := os.LookupEnv("LOG_LEVEL")
	if !ok {
		os.Setenv("LOG_LEVEL", "info")
		logLevel = "info"
	}

	defaultLogLevel := zapcore.InfoLevel

	switch logLevel {
	case "debug":
		defaultLogLevel = zapcore.DebugLevel
	case "warn":
		defaultLogLevel = zapcore.WarnLevel
	case "error":
		defaultLogLevel = zapcore.ErrorLevel
	case "dpanic":
		defaultLogLevel = zapcore.DPanicLevel
	case "panic":
		defaultLogLevel = zapcore.PanicLevel
	case "fatal":
		defaultLogLevel = zapcore.FatalLevel
	default:
		defaultLogLevel = zapcore.InfoLevel
	}

	isDebug, ok := os.LookupEnv("DEBUG_MODE")
	if !ok {
		os.Setenv("DEBUG_MODE", "off")
		isDebug = "off"
	}
	if isDebug != "off" && isDebug != "false" {
		defaultLogLevel = zapcore.DebugLevel
	}

	zapConfig := zap.NewProductionEncoderConfig()
	zapConfig.EncodeTime = zapcore.ISO8601TimeEncoder
	// fileEncoder := zapcore.NewJSONEncoder(zapConfig)
	//consoleEncoder := zapcore.NewConsoleEncoder(zapConfig)
	consoleEncoder := zapcore.NewJSONEncoder(zapConfig)
	//logFile, _ := os.OpenFile("kueden-api-log.json", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	//writer := zapcore.AddSync(logFile)
	// writer := zapcore.AddSync(&lumberjack.Logger{
	// 	Filename:   "cid.log.json",
	// 	MaxSize:    128, // megabytes
	// 	MaxBackups: 3,
	// 	MaxAge:     28, // days
	// 	Compress:   true,
	// })

	core := zapcore.NewTee(
		// zapcore.NewCore(fileEncoder, writer, defaultLogLevel),
		zapcore.NewCore(consoleEncoder, zapcore.AddSync(os.Stdout), defaultLogLevel),
	)
	Logger = zap.New(core, zap.AddCaller(), zap.AddStacktrace(zapcore.ErrorLevel))
}
