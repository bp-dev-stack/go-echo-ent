package component

import (
	"context"
	"database/sql"
	"fmt"
	"go-echo-ent/ent"
	"sync"
	"time"

	"entgo.io/ent/dialect"
	entsql "entgo.io/ent/dialect/sql"
	_ "github.com/jackc/pgx/v5/stdlib"
)

var (
	entOnce   sync.Once
	entClient *ent.Client
)

func GetEntClient() *ent.Client {
	entOnce.Do(func() {
		Logger.Info("Initializing Ent Repository Client")

		var err error
		entClient, err = newEntClient()
		if err != nil {
			Logger.Fatal("Unable to create Ent Repository Client: " + err.Error())
		}

		Logger.Info("Ent Repository Client initialization completed")
	})

	return entClient
}

func getDbUrl(config *Config) string {
	return fmt.Sprintf("postgresql://%s:%s@%s:%d/%s?%s",
		config.DB.User,
		config.DB.Password,
		config.DB.Host,
		config.DB.Port,
		config.DB.Name,
		config.DB.Option,
	)
}

func openDB(databaseUrl string) (*ent.Client, error) {
	db, err := sql.Open("pgx", databaseUrl)
	if err != nil {
		Logger.Fatal(err.Error())
	}

	db.SetMaxIdleConns(10)
	db.SetMaxOpenConns(100)
	db.SetConnMaxLifetime(time.Hour)

	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	//check DB by ping
	err = db.PingContext(ctx)
	if err != nil {
		return nil, err
	}

	// Create an ent.Driver from `db`.
	drv := entsql.OpenDB(dialect.Postgres, db)
	return ent.NewClient(ent.Driver(drv)), err
}

func newEntClient() (*ent.Client, error) {
	client, err := openDB(getDbUrl(GetAppConfig()))
	if err != nil {
		Logger.Fatal("failed opening connection to database: " + err.Error())
	}

	return client, err
}

func InitializeRepository() error {
	dbClient := GetEntClient()

	// Return result of auto
	return dbClient.Schema.Create(context.Background())
}
