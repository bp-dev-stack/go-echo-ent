package component

import (
	"strings"
	"sync"

	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

var (
	echoOnce     sync.Once
	echoInstance *echo.Echo
)

func GetEchoInstance() *echo.Echo {
	echoOnce.Do(func() {
		Logger.Info("Initializing Echo Web Application Server")
		echoInstance = echo.New()
		echoInstance.HideBanner = true
		echoInstance.Debug = GetAppConfig().Server.Debug
		echoInstance.Use(middleware.RequestIDWithConfig(middleware.RequestIDConfig{
			Generator: func() string {
				return strings.Replace(uuid.New().String(), "-", "", -1)
			},
		}))

		Logger.Info("Echo Web Application Server initialization completed")
	})

	return echoInstance
}
