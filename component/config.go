package component

import (
	"os"
	"strconv"
	"sync"
)

type Config struct {
	DB     DBConfig
	Server EchoServerConfig
}

type DBConfig struct {
	Host     string
	Port     int
	Name     string
	User     string
	Password string
	Option   string
}

type EchoServerConfig struct {
	Listen   string
	Port     int
	LogLevel string
	Debug    bool
}

var (
	configOnce sync.Once
	appConfig  *Config
)

var DefaultConfig = Config{
	DB: DBConfig{
		Host: "127.0.0.1",
		Port: 5432,
		Name: "kedb",
		User: "kueden",
	},
	Server: EchoServerConfig{
		Listen:   "0.0.0.0",
		Port:     8765,
		LogLevel: "info",
		Debug:    false,
	},
}

func GetAppConfig() *Config {
	configOnce.Do(func() {
		appConfig = &Config{}
		Logger.Info("Initializing Application Environment")

		// Echo Server Environment Variables
		serverListen, ok := os.LookupEnv("HTTP_LISTEN")
		if !ok {
			Logger.Warn("Failed to retrieve HTTP_LISTEN environment variable. Setting to default value 0.0.0.0")
			os.Setenv("HTTP_LISTEN", "0.0.0.0")
			serverListen = "0.0.0.0"
		}
		appConfig.Server.Listen = serverListen

		svrPortStr, ok := os.LookupEnv("HTTP_PORT")
		if !ok {
			Logger.Warn("Failed to retrieve HTTP_PORT environment variable. Setting to default value 8765")
			os.Setenv("HTTP_PORT", "8765")
			svrPortStr = "8765"
		}
		serverPort, err := strconv.Atoi(svrPortStr)
		if err != nil {
			Logger.Warn("Unable to properly parse HTTP_PORT environment variable. Setting to default value 8765")
			serverPort = 8765
		}
		appConfig.Server.Port = serverPort

		logLevel, ok := os.LookupEnv("LOG_LEVEL")
		if !ok {
			Logger.Warn("Failed to retrieve LOG_LEVEL environment variable. Setting to default value info")
			os.Setenv("LOG_LEVEL", "info")
			logLevel = "info"
		}
		appConfig.Server.LogLevel = logLevel

		isDebug, ok := os.LookupEnv("DEBUG_MODE")
		if !ok {
			Logger.Warn("Failed to retrieve DEBUG_MODE environment variable. Setting to default value off=false")
			os.Setenv("DEBUG_MODE", "off")
			isDebug = "off"
		}
		if isDebug != "off" && isDebug != "false" {
			appConfig.Server.Debug = true
		}

		// DB Server Environment Variables
		dbHost, ok := os.LookupEnv("DB_HOST")
		if !ok {
			Logger.Warn("Failed to retrieve DB_HOST environment variable. Setting to default value 127.0.0.1")
			os.Setenv("DB_HOST", "127.0.0.1")
			dbHost = "127.0.0.1"
		}
		appConfig.DB.Host = dbHost

		dbPortStr, ok := os.LookupEnv("DB_PORT")
		if !ok {
			Logger.Warn("Failed to retrieve DB_PORT environment variable. Setting to default value 5432")
			os.Setenv("DB_PORT", "5432")
			dbPortStr = "5432"
		}
		dbPort, err := strconv.Atoi(dbPortStr)
		if err != nil {
			Logger.Warn("Unable to properly parse DB_PORT environment variable. Setting to default value 5432")
			dbPort = 5432
		}
		appConfig.DB.Port = dbPort

		dbName, ok := os.LookupEnv("DB_NAME")
		if !ok {
			Logger.Warn("Failed to retrieve DB_NAME environment variable. Setting to default value")
			os.Setenv("DB_NAME", "kedb")
			dbName = "kedb"
		}
		appConfig.DB.Name = dbName

		dbUser, ok := os.LookupEnv("DB_USER")
		if !ok {
			Logger.Warn("Failed to retrieve DB_USER environment variable. Setting to default value")
			os.Setenv("DB_USER", "kueden")
			dbUser = "kueden"
		}
		appConfig.DB.User = dbUser

		dbPassword, ok := os.LookupEnv("DB_PASS")
		if !ok {
			Logger.Warn("Failed to retrieve DB_PASS environment variable. Setting to default value")
			os.Setenv("DB_PASS", "Set_2_a_very_secure_phrase")
			dbPassword = "Set_2_a_very_secure_phrase"
		}
		appConfig.DB.Password = dbPassword

		dbOption, ok := os.LookupEnv("DB_OPT")
		if !ok {
			Logger.Warn("Failed to retrieve DB_OPT environment variable. Setting to default value")
			os.Setenv("DB_OPT", "")
		}
		appConfig.DB.Option = dbOption

		Logger.Info("Application Environment initialization completed")
	})

	return appConfig
}

func (c *Config) SetDB(host string, port int, name string, user string, password string, option string) *Config {
	c.DB = DBConfig{
		Host:     host,
		Port:     port,
		Name:     name,
		User:     user,
		Password: password,
		Option:   option,
	}

	return c
}

func (c *Config) SetServer(listen string, port int, logLevel string, debug bool) *Config {
	c.Server = EchoServerConfig{
		Listen:   listen,
		Port:     port,
		LogLevel: logLevel,
		Debug:    debug,
	}

	return c
}
