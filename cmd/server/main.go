package main

import (
	"fmt"
	component "go-echo-ent/component"
	"go-echo-ent/router"
	v1router "go-echo-ent/router/v1"
)

func main() {
	var err error
	// Initialize Logger
	component.InitializeLogger()
	logger := component.Logger
	logger.Info("Initializing Ku Eden API Server")
	cfg := component.GetAppConfig()
	e := component.GetEchoInstance()
	err = component.InitializeRepository()
	if err != nil {
		logger.Fatal("Repository Initialization failed" + err.Error())
	}

	// Register API Routes
	logger.Info("Registering API Routes")
	router.RegisterUniversalRoute(e)
	v1router.RegisterV1Route(e)
	logger.Info("API Routes registration completed")

	logger.Info("Starting Ku Eden API Server")
	err = e.Start(fmt.Sprintf("%s:%d", cfg.Server.Listen, cfg.Server.Port))
	if err != nil {
		logger.Fatal("Fail to Start Application Server: " + err.Error())
	}
}
