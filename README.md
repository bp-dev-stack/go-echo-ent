# Go Application Template: Echo + Ent

This template is a baseline for creating API server written in go. THe following framework are used as the basic stack:

- Web Framework: Echo
- Entity Framework: Ent
- Database: PostgreSQL
- Logging: Zap
- Configuration: OS Environment + Viper

## Usage

1. Clone the repository

   ```bash
   # git clone https://gitlab.com/bp-dev-stack/go-echo-ent.git && git remote rm origin
   ```

2. Rename folder and remove linkage to existing 'origin'

   ```bash
   # mv go-echo-ent <your_project_name>
   # cd <your_project_name>
   # git remote rm origin
   ```

3. Rename go module and install dependencies

    ```bash
    # go mod edit -module <your_go_module_name>
    # go mod tidy
    # go get ./...
    ```

4. Establish new repository and set your new origin repository

    ```bash
    # git add .
    # git commit -m "new app baseline"
    # git remote add origin <your_new_remote_git_uri>
    # git push -u origin main
    ```

5. Start developing your new awesome API
