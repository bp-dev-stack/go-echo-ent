package v1

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

func GetV1Index(c echo.Context) error {
	return c.String(http.StatusOK, "V1 API - Root")
}
