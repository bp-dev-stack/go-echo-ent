package v1

import (
	"go-echo-ent/component"
	v1handler "go-echo-ent/handler/v1"

	"github.com/labstack/echo/v4"
)

func RegisterV1Route(e *echo.Echo) error {
	var err error = nil

	component.Logger.Info("Registering V1 Routes")

	v1 := e.Group("/v1")
	v1.GET("", v1handler.GetV1Index)

	// sub route - price
	// err = RegisterV1XXXRoute(v1)
	// err = RegisterV1YYYRoute(v1)

	component.Logger.Info("V1 Routes registration completed")

	return err
}
