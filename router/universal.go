package router

import (
	"go-echo-ent/component"
	"net/http"

	"github.com/labstack/echo/v4"
)

func RegisterUniversalRoute(e *echo.Echo) error {
	var err error = nil

	component.Logger.Info("Registering Universal Routes")

	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Assalamualaikum Dunia!")
	})

	component.Logger.Info("Universal Routes registration completed")

	return err
}
